import time

list = open('listNumber', 'r')
contenuList = list.read()

def trie(T):
    if T == []:
        return []
    else:
        n = len(T) - 1
        T1 = []
        T2 = []
        for k in range(1, n + 1):
            if int(T[k]) <= int(T[0]):
                T1.append(T[k])
            else:
                T2.append(T[k])

    T = trie(T1) + [T[0]] + trie(T2)
    return T

def mySplit(contenu,operateur ):
    number = []
    current_number = ""
    for char in contenu:
        if char == ""+operateur+"":
            number.append(current_number)
            current_number = ""
        else:
            current_number += char
    number.append(current_number)
    return number


if __name__ == '__main__':
    debut = time.time()
    table = mySplit(contenuList," ")
    # par trie rapide
    print(trie(table))
    fin = time.time()
    print(fin - debut)
