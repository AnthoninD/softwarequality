import unittest

def mySplit(contenu, operateur):
    number = []
    current_number = ""
    for char in contenu:
        if char == operateur:
            if current_number:
                number.append(current_number)
                current_number = ""
        else:
            current_number += char
    if current_number:
        number.append(current_number)
    return number


class TestSplit(unittest.TestCase):

    def test_string_split(self):
        s = 'hello world'
        self.assertEqual(mySplit(s," "), ['hello', 'world'])
        self.assertTrue(type(mySplit(s," ")),list)
        self.assertEqual(len(mySplit(s," ")),2)

    def test_double_operateur(self):
        p = 'asdaz,,adzaz'
        self.assertTrue(type(mySplit(p,",")), list)
        self.assertEqual(len(mySplit(p,",")), 2)


if __name__ == '__main__':
    unittest.main()