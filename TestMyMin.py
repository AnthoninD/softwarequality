import unittest

def myMin(array):
    arrLength = len(array)
    for number in range(arrLength - 1, 0, -1):
        for item in range(number):
            if array[item] > array[item + 1]:
                array[item], array[item+1] = array[item+1], array[item]
    return array[0]




class minTest(unittest.TestCase):

    def test_myMin_simple(self):
        self.assertEqual(myMin([1, 2, 3]), 1)

    def test_myMin_egal(self):
        self.assertEqual(myMin([1, 1, 1]), 1)

    def test_myMin_neg(self):
        self.assertEqual(myMin([-1, 1, 1]), -1)

    def test_myMin_alphabet(self):
        self.assertEqual(myMin(["r", "b", "a"]), "a")






if __name__ == '__main__':
   unittest.main()
