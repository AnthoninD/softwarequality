import time

list = open('listNumber', 'r')
contenuList = list.read()

def trie(T):
    if T == []:
        return []
    else:
        n = len(T) - 1
        T1 = []
        T2 = []
        for k in range(1, n + 1):
            if int(T[k]) <= int(T[0]):
                T1.append(T[k])
            else:
                T2.append(T[k])

    T = trie(T1) + [T[0]] + trie(T2)
    return T


def mySplit(contenu,operateur ):
    number = []
    current_number = ""
    for char in contenu:
        if char == ""+operateur+"":
            number.append(current_number)
            current_number = ""
        else:
            current_number += char
    number.append(current_number)
    return number


def bubbleSort(arr):
    arrLength = len(arr)
    for number in range(arrLength-1,0,-1):
        for item in range(number):
            if int(arr[item]) > int(arr[item + 1]):
                swapPositions(arr, item, item + 1)
    return arr

def swapPositions(list, pos1, pos2):
    list[pos1], list[pos2] = list[pos2], list[pos1]
    return list


if __name__ == '__main__':
    debut = time.time()
    table = mySplit(contenuList," ")
    # par trie rapide
    print(trie(table))
    # par bubbleSort
    #print(bubbleSort(table))
    fin = time.time()
    print(fin - debut)
